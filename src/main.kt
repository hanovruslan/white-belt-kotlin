import evolaze.kotlin.MAX
import evolaze.kotlin.MIN
import evolaze.kotlin.factorial

fun main (args: Array<String>) {
    println("Please, enter a number between $MIN and $MAX")
    val param = readLine()!!.toInt()
    val result = factorial(param)
    println("Result is $result")
}