package evolaze.kotlin

val MIN = 1
val MAX = 10

fun factorial (param: Int): Int {
    var result = 1
    if (param >= MIN + 1 && param <= MAX) {
        for(i in MIN + 1..param) {
            result *= i
        }
    }

    return result
}
